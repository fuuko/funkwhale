# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _

from .models import User


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


@admin.register(User)
class UserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    list_display = [
        'username',
        'email',
        'date_joined',
        'last_login',
        'is_staff',
        'is_superuser',
    ]
    list_filter = [
        'is_superuser',
        'is_staff',
        'privacy_level',
        'permission_settings',
        'permission_library',
        'permission_federation',
    ]

    fieldsets = (
        (None, {'fields': ('username', 'password', 'privacy_level')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'permission_upload',
                'permission_library',
                'permission_settings',
                'permission_federation')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Useless fields'), {
            'fields': (
                'user_permissions',
                'groups',
            )})
        )
