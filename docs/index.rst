.. funkwhale documentation master file, created by
   sphinx-quickstart on Sun Jun 25 18:49:23 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Funkwhale's documentation
=====================================

Funkwhale is a self-hosted, modern free and open-source music server, heavily inspired by Grooveshark.

.. toctree::
   :maxdepth: 2

   users/index
   features
   installation/index
   configuration
   importing-music
   federation
   api
   upgrading
   third-party
   contributing
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
