Funkwhale
=============

.. image:: ./front/src/assets/logo/logo-full-500.png
  :alt: Funkwhale logo
  :target: https://funkwhale.audio

A self-hosted tribute to Grooveshark.com.

LICENSE: BSD

Getting help
------------

We offer various Matrix.org rooms to discuss about Funkwhale:

- `#funkwhale:matrix.org <https://riot.im/app/#/room/#funkwhale:matrix.org>`_ for general questions about funkwhale
- `#funkwhale-dev:matrix.org <https://riot.im/app/#/room/#funkwhale-dev:matrix.org>`_ for development-focused discussion

Please join those rooms if you have any questions!

You can also contact `@funkwhale@mastodon.eliotberriot.com <https://mastodon.eliotberriot.com/@funkwhale>`_ on the fediverse.


Contribute
----------

Contribution guidelines as well as development installation instructions
are outlined in `CONTRIBUTING <CONTRIBUTING>`_
