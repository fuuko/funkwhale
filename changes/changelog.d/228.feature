New action table component for quick and efficient batch actions (#228)
This is implemented on the federated tracks pages, but will be included
in other pages as well depending on the feedback.
