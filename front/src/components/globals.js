import Vue from 'vue'

import HumanDate from '@/components/common/HumanDate'

Vue.component('human-date', HumanDate)

import Username from '@/components/common/Username'

Vue.component('username', Username)

import DangerousButton from '@/components/common/DangerousButton'

Vue.component('dangerous-button', DangerousButton)

export default {}
